﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using FileDistribution.Models;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace FileDistribution.Controllers
{
    //[Authorize]
    [Produces("application/json")]
    [Route("api/downloadAPK")]
    public class FileController : Controller
    {

        [HttpGet("List")]
        public IEnumerable<ApkFile> GetListApkFiles()
        {
            var ret = new List<ApkFile>();
            try
            {
                string dir = @"\apks";
                DirectoryInfo d = new DirectoryInfo(dir);//Assuming Test is your Folder
                FileInfo[] Files = d.GetFiles("*.apk"); //Getting Text files

                string str = "";

                foreach (FileInfo file in Files)
                {
                    ApkFile apk = new ApkFile();
                    str = str + ", " + file.Name;
                    apk.Name = file.Name;
                    apk.Url = "http://geo-direct.ai/jingandownload/apks/" + file.Name;
                    apk.BuildNumber = int.Parse(Regex.Match(file.Name, @"\d+").Value); 
                    apk.DateCreated = file.CreationTime;

                    ret.Add(apk);
                }

                ret = ret.OrderByDescending(_ => _.DateCreated).ToList();

            }
            catch (Exception ex)
            {


            }
            return ret;
        }


    
    }
}