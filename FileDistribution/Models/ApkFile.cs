﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Policy;
using System.Threading.Tasks;

namespace FileDistribution.Models
{
    public class ApkFile
    {
        public string Name;
        public DateTime DateCreated;
        public int BuildNumber;
        public string Url;
    }
}
